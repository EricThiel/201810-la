# Basic pod info

![](assets/images/dne-dci-dcloud.png)

| Hostname | Description | IP Address | Credentials |
| --- | --- | --- | --- |
| **wkst1** | Dev Workstation: Windows | 198.18.133.36 | dcloud\demouser/C1sco12345 |
| **ubuntu** | Dev Workstation: Linux | 198.18.134.28 | cisco/C1sco12345 |
| **nxosv-1** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.140 | admin/C1sco12345 |
| **nxosv-2** | Nexus 9000v - 7.0.3(I2).1 | 198.18.134.141 | admin/C1sco12345 |
| **apic1** | ACI Simulator - 2.1.1h | 198.18.133.200 | admin/C1sco12345 |
| **centos1** | CentOS 7 Server - Automation Target | 198.18.134.49 | root/C1sco12345 or demouser/C1sco12345 |
| **centos2** | CentOS 7 Server - Automation Target | 198.18.134.50 | root/C1sco12345 or demouser/C1sco12345 |
| **ios-xe-mgmt.cisco.com** | CSR1000v - 16.08.01 | ios-xe-mgmt.cisco.com:8181 | root/D_Vay!_10& |
| ucsctl1 | UCS Central - 1.5(1b) | 198.18.133.90 | admin/C1sco12345 |
| ucsm1 | UCS Manager Emulator | 198.18.133.91 | admin/C1sco12345 |
| ucsd | UCS Director - 6.0.1.1 | 198.18.133.112 | admin/C1sco12345 |
| win2012r2 | Windows 2012 Standard Server - Automation Target | 198.18.133.20 | dcloud\administrator/C1sco12345 |
| vc1 | vCenter 6.0 | 198.18.133.30 | administrator@vsphere.local/C1sco12345! |
| vesx1 | vSphere Host | 198.18.133.31 | root/C1sco12345 |
| vesx2 | vSphere Host | 198.18.133.32 | root/C1sco12345 |
| cimc1 | UCS IMC Emulator | 198.18.134.88 | root/C1sco12345 or admin/C1sco12345 |
| na-edge1 | vNetApp | 198.18.133.115 | root/C1sco12345 |
| ad1 | Domain Controller | 198.18.133.1 | administrator/C1sco12345 |


## Getting connected

1. Run Anyconnect client and fill in url with ```dcloud-sjc-anyconnect.cisco.com``` 

1. Click connect, and log in with the credentials from your assigned pod, listed below:

| Session Id | Session Name | Usernames | Password | Pod URL |
| --- | --- | --- | ---  | --- |
| 119966 | 1 - Cisco DevNet Express Data Center v2 | v875user1 | d0116a | [https://128.107.66.246:8443](https://v875user1:d0116a@128.107.66.246:8443) |
| 119965 | 2 - Cisco DevNet Express Data Center v2 | v292user1 | 332e1e | [https://128.107.66.248:8443](https://v292user1:332e1e@128.107.66.248:8443) |
| 119964 | 3 - Cisco DevNet Express Data Center v2 | v1191user1 | e10448 | [https://128.107.66.250:8443](https://v1191user1:e10448@128.107.66.250:8443) |
| 119963 | 4 - Cisco DevNet Express Data Center v2 | v1437user1 | 090b33 | [https://128.107.222.248:8443](https://v1437user1:090b33@128.107.222.248:8443) |
| 119962 | 5 - Cisco DevNet Express Data Center v2 | v293user1 | 34664a | [https://128.107.66.160:8443](https://v293user1:34664a@128.107.66.160:8443) |
| 119961 | 6 - Cisco DevNet Express Data Center v2 | v1054user1 | 68804e | [https://128.107.66.163:8443](https://v1054user1:68804e@128.107.66.163:8443) |
| 119960 | 7 - Cisco DevNet Express Data Center v2 | v1232user1 | 9b5957 | [https://128.107.66.167:8443](https://v1232user1:9b5957@128.107.66.167:8443) |
| 119959 | 8 - Cisco DevNet Express Data Center v2 | v515user1 | 88ec89 | [https://128.107.66.169:8443](https://v515user1:88ec89@128.107.66.169:8443) |
| 119958 | 9 - Cisco DevNet Express Data Center v2 | v702user1 | 0c40ea | [https://128.107.66.172:8443](https://v702user1:0c40ea@128.107.66.172:8443) |
| 119957 | 10 - Cisco DevNet Express Data Center v2 | v25user1 | 908e06 | [https://128.107.66.178:8443](https://v25user1:908e06@128.107.66.178:8443) |
| 119956 | 11 - Cisco DevNet Express Data Center v2 | v197user1 | 71872a | [https://128.107.66.182:8443](https://v197user1:71872a@128.107.66.182:8443) |
| 119955 | 12 - Cisco DevNet Express Data Center v2 | v215user1 | e7434f | [https://128.107.66.57:8443](https://v215user1:e7434f@128.107.66.57:8443) |
| 119954 | 13 - Cisco DevNet Express Data Center v2 | v866user1 | f2f3e7 | [https://128.107.66.184:8443](https://v866user1:f2f3e7@128.107.66.184:8443) |
| 119953 | 14 - Cisco DevNet Express Data Center v2 | v403user1 | e98a32 | [https://128.107.66.198:8443](https://v403user1:e98a32@128.107.66.198:8443) |
| 119952 | 15 - Cisco DevNet Express Data Center v2 | v797user1 | 573dba | [https://128.107.66.252:8443](https://v797user1:573dba@128.107.66.252:8443) |
| 119951 | 16 - Cisco DevNet Express Data Center v2 | v243user1 | ed732f | [https://128.107.66.253:8443](https://v243user1:ed732f@128.107.66.253:8443) |
| 119950 | 17 - Cisco DevNet Express Data Center v2 | v389user1 | 2a7deb | [https://128.107.66.255:8443](https://v389user1:2a7deb@128.107.66.255:8443) |
| 119949 | 18 - Cisco DevNet Express Data Center v2 | v963user1 | 01e15c | [https://128.107.219.4:8443](https://v963user1:01e15c@128.107.219.4:8443) |
| 119948 | 19 - Cisco DevNet Express Data Center v2 | v1495user1 | e00ed7 | [https://128.107.219.5:8443](https://v1495user1:e00ed7@128.107.219.5:8443) |
| 119947 | 20 - Cisco DevNet Express Data Center v2 | v1285user1 | 05365f | [https://128.107.219.7:8443](https://v1285user1:05365f@128.107.219.7:8443) |

1. Once VPNed into your pod, run your local RDP client, and connect to 198.18.133.36 as dcloud\demouser with a password of C1sco12345
    * Once the scripts finish auto-running, you may disconnect this session
1. Open your local VNC client and connect to 198.18.134.28:5901
1. On the desktop, double-click `Terminal for coding`. When prompted, select `2` for Python2
1. Change up one directory with `cd ..` so that you are in the `~/CiscoDevNet/code` directory
1. Run `git clone https://gitlab.com/securenetwrk/201810-la.git` to download the latest code for the workshop
    * At any point if more code is uploaded, you can run a `git pull` from within the `201810-la` folder to get the latest files
1. Change into the `201810-la` folder `cd 201810-la`






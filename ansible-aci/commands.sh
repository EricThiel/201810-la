ansible-playbook 01_aci_tenant_pb.yml
ansible-playbook 02_aci_tenant_network_pb.yml -e vrf=prod_vrf
ansible-playbook 03_aci_tenant_policies_pb.yml -vvv
ansible-playbook 04_aci_tenant_app_pb.yml --extra-vars "@./vars/intranet_vars.yml"
ansible-playbook 05_aci_deploy_app.yml --extra-vars "@./vars/intranet_vars_full_config.yml"
ansible-playbook 06_aci_rest_pb.yml
ansible-playbook 07_lab_cleanup.yml

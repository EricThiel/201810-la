# Programmability Workshop - October 2018

Welcome to the 3-day programmability workshop hosted by Cisco. We will be using the following documentation over the course of the hands-on excercises, as well as using many modules from [Learning Labs](https://learninglabs.cisco.com/tracks). 

## Welcome, Intros, setting up dev environment

## Intro and Setup
### [Lab Setup](0-Setup.md)

## Intro to Ansible
### [Intro to Ansible LM-1451](https://learninglabs.cisco.com/tracks/devnet-express-dci)

## Ansible for IOS-XE
### [Ansible for IOS-XE](https://learninglabs.cisco.com/modules/intro-ansible-iosxe)

## Ansible for ASA
### [Ansible for ASA](1-ASA.md)

## Ansible for NX-OS
### [LM-5601 -> Intro to Ansible](https://learninglabs.cisco.com/tracks/devnet-express-dci)

## Ansible for ACI

## IOS-XE on-box code demo


